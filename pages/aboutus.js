import "../styles/style.css";
import Nav from "../components/Nav";
import Footer from "../components/Footer";
import React, { Component } from "react";
import ReactGA from "react-ga";


export default class extends Component {

  componentWillMount() {
    ReactGA.initialize("UA-131193519-1");
    ReactGA.pageview("/aboutus");
  }
  render() {
    return (
      <div>
        <Nav />
        <div
          className="bg-blue-darkest text-white"
          style={{ height: "300px" }}
        >
          <div className="container mx-auto py-20">
            <div className="flex flex-col justify-center items-center md:flex-row">
              <h2> About Us</h2>
            </div>
          </div>
        </div>
        <div className="container mx-auto px-6 py-10 flex justify-center">
          <div className="w-3/4">
            <p
              style={{
                textAlign: "justify",
                lineHeight: "1.5",
                fontSize: "23px"
              }}
            >
              KYC is brought to you by Civic Monitor, a Nigerian non-profit
              organization committed to empowering citizens and driving
              engagement by providing the tools and data to help citizens
              make more thoughtful decisions.
            </p>
            <br />
            <p
              style={{
                textAlign: "justify",
                lineHeight: "1.5",
                fontSize: "23px"
              }}
            >
              Our target with KYC is to democratize information about
              candidates vying for political offices in Nigeria. With over
              90 registered political parties in the country, it can be
              overwhelming for eligible voters to keep track of where
              candidates stand on the most important issues.{" "}
            </p>
            <br />

            <p
              style={{
                textAlign: "justify",
                lineHeight: "1.5",
                fontSize: "23px"
              }}
            >
              Some political parties have more resources than others, and
              this gives them an advantage in communicating their programs
              and ideas. KYC levels the playing field, such that every
              candidate has a shot at being heard. We provide data on the
              track records and issue positions of candidates in a fair,
              representative and easy to understand format so that voters
              interested in learning more can easily do so.
            </p>
            <br />
            <p
              style={{
                textAlign: "justify",
                lineHeight: "1.5",
                fontSize: "23px"
              }}
            >
              {" "}
              We also provide the tools for voters to compare the different
              positions of two candidates on any particular issue.{" "}
            </p>

            <p
              style={{
                paddingTop: "50px",
                textAlign: "justify",
                lineHeight: "2",
                fontSize: "23px"
              }}
            >
              Are you interested in using Civic Monitor's candidate
              information for your own apps or software? <br />
              Contact us at
              <strong>
                {" "}
                <a href="mailto:kyc@civicmonitor.com" target="_top">kyc@civicmonitor.com</a>
              </strong>
            </p>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}
