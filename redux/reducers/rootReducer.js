import { combineReducers } from "redux";
import compareReducer from "./CompareReducer"

const rootReducer = combineReducers({
  compareReducer
})

export default rootReducer