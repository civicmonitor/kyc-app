const express = require("express");
const path = require("path");
const next = require("next");
const cache = require('lru-cache'); // for using least-recently-used based caching

var port = process.env.PORT || 3000;

const app = next({ dev: process.env.NODE_ENV !== "production" });
const handle = app.getRequestHandler();
const robotsOptions = {
  root: __dirname + '/static/',
  headers: {
    'Content-Type': 'text/plain;charset=UTF-8',
  }
}

const ssrCache = new cache({
  max: 20, // not more than 20 results will be cached
  maxAge: 1000 * 60 * 5 // 5 mins
});



app
  .prepare()
  .then(() => {
    const server = express();

    // server.get("/service-worker.js", (req, res) => {
    //   // Don't cache service worker is a best practice (otherwise clients wont get emergency bug fix)
    //   res.set(
    //     "Cache-Control",
    //     "no-store, no-cache, must-revalidate, proxy-revalidate"
    //   );
    //   res.set("Content-Type", "application/javascript");
    //   // if (dev) {
    //   //     console.log('dev - sw from static');
    //   //     app.serveStatic(req, res, path.resolve("./static/service-worker.js"));
    //   // } else {
    //   console.log("next - sw from next");
    //   app.serveStatic(req, res, path.resolve("./.next/service-worker.js"));
    //   // }
    // });

    server.get("/loaderio-636e91d99a347403c6e6db1cee6b0c7d", (req, res) =>
      res
        .status(200)
        .sendFile(
          "loaderio-636e91d99a347403c6e6db1cee6b0c7d.txt",
          robotsOptions
        )
    );

    server.get('/', (req, res) => {
      renderAndCache(req, res, '/');
    });
    server.get('/aboutus', (req, res) => {
      renderAndCache(req, res, '/aboutus');
    });
    

      server.get('/profile/:id', (req, res) => {
          const actualPage = '/profile'
          const queryParams = { id: req.params.id }
        renderAndCache(req, res, actualPage, queryParams);

      })

      
      server.get('/compare/:candidateA/:candidateB/:issue', (req, res) => {
          const actualPage = '/compare'
          const queryParams = { candidateA: req.params.candidateA, candidateB: req.params.candidateB, issue: req.params.issue };
        renderAndCache(req, res, actualPage, queryParams);
      })


    server.get("*", (req, res) => {
      return handle(req, res);
    });

    server.listen(port, err => {
      if (err) throw err;
      console.log(`> Ready on http://localhost:${port}`);
    });
  })
  .catch(ex => {
    console.error(ex.stack);
    process.exit(1);
  });


async function renderAndCache(req, res, pagePath, queryParams) {
  const key = req.url;

  // if page is in cache, server from cache
  if (ssrCache.has(key)) {
    res.setHeader('x-cache', 'HIT');
    res.send(ssrCache.get(key));
    return;
  }

  try {
    // if not in cache, render the page into HTML
    const html = await app.renderToHTML(req, res, pagePath, queryParams);

    // if something wrong with the request, let's skip the cache
    if (res.statusCode !== 200) {
      res.send(html);
      return;
    }

    ssrCache.set(key, html);

    res.setHeader('x-cache', 'MISS');
    res.send(html);
  } catch (err) {
    app.renderError(err, req, res, pagePath, queryParams);
  }
}

