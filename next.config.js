// next.config.js
const withPlugins = require("next-compose-plugins");

const withCSS = require("@zeit/next-css");
const withOffline = require("next-offline");

const nextConfig = {
  webpack: (config, options) => {
    // modify the `config` here

    return config;
  }
};

module.exports = withPlugins(
  [
    withCSS
//     [withOffline,
//     {
//       workboxOpts: {
//         clientsClaim: true,
//         skipWaiting: true,
//         offlineGoogleAnalytics: true,
//         globPatterns: ["static/**/*"],
//         globDirectory: ".",
//         runtimeCaching: [
//           {
//             urlPattern: new RegExp("https:/api.civicmonitor.com"),
//             handler: "networkFirst",
//             options: {
//               cacheableResponse: {
//                 statuses: [0, 200],
//               }
//             }
//           },
//           {
//             urlPattern: new RegExp("https://res.cloudinary.com/civic-monitor"),
//             handler: "networkFirst"
//           }
//         ]
//       }
//     }]
  ],

  nextConfig
);


